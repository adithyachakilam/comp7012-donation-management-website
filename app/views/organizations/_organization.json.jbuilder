json.extract! organization, :id, :name, :owner, :website, :address, :year, :about, :created_at, :updated_at
json.url organization_url(organization, format: :json)
