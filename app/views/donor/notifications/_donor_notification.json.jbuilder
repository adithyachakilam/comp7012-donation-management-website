json.extract! donor_notification, :id, :title, :content, :created_at, :updated_at
json.url donor_notification_url(donor_notification, format: :json)
