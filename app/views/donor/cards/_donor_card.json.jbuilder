json.extract! donor_card, :id, :cardNumber, :cvv, :expiryMonth, :expiryYear, :created_at, :updated_at
json.url donor_card_url(donor_card, format: :json)
