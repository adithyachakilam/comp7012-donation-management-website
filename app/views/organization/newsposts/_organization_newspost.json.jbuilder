json.extract! organization_newspost, :id, :title, :content, :image, :created_at, :updated_at
json.url organization_newspost_url(organization_newspost, format: :json)
