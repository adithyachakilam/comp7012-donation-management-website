class CardsController < ApplicationController
  before_action :set_cards
  before_action :set_card, only: [:show, :edit, :update, :destroy]

  # GET donors/1/cards
  def index
    @cards = @donor.cards
  end

  # GET donors/1/cards/1
  def show
  end

  # GET donors/1/cards/new
  def new
    @card = @donor.cards.build
  end

  # GET donors/1/cards/1/edit
  def edit
  end

  # POST donors/1/cards
  def create
    @card = @donor.cards.build(card_params)

    if @card.save
      redirect_to([@card.donor, @card], notice: 'Card was successfully created.')
    else
      render action: 'new'
    end
  end

  # PUT donors/1/cards/1
  def update
    if @card.update_attributes(card_params)
      redirect_to([@card.donor, @card], notice: 'Card was successfully updated.')
    else
      render action: 'edit'
    end
  end

  # DELETE donors/1/cards/1
  def destroy
    @card.destroy

    redirect_to donor_cards_url(@donor)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cards
      @donor = Donor.find(params[:donor_id])
    end

    def set_card
      @card = @donor.cards.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def card_params
      params.require(:card).permit(:cardNumber, :cvv, :expiryMonth, :expiryYear)
    end
end
