class HomeController < ApplicationController
  
  def index
    @organizations = Organization.paginate(:page => params[:page], :per_page => 2)
    @tags = Tag.all
    if user_signed_in?
      if current_user.organization?
        @organization = Organization.find_by user_id: current_user.id
        if @organization != nil
          redirect_to org_landing_url(@organization.id)
        else
          redirect_to new_organization_url
        end
      elsif current_user.donor?
        @donor = Donor.find_by user_id: current_user.id
        if @donor != nil
          redirect_to donor_url(@donor.id)
        else
          redirect_to new_donor_url
        end
      end
    end
  end
end