class PaymentsController < ApplicationController


    def new
      @payment = Payment.where(organization_id: params[:id], donor_id: params[:donorid]).new
      #render payments/new.html.erb
    end

    def edit
      @payment = Payment.find(params[:id])
      #render payments/edit.html.erb
    end
   
    # POST /payments
    def create
      @donor = Donor.find(params[:payment][:donor_id]) # so that we can still display the nav menu
      @payment = Payment.new(amount: params[:payment][:amount],
                                  paymentType: params[:payment][:paymentType],
                                  donor_id: params[:payment][:donor_id],
                                  organization_id: params[:payment][:organization_id])
        
        if @payment.save
           flash[:notice] = "Payment saved successfully!"
           redirect_to :controller => 'donors', :action => 'donor_home', :id => @payment.donor_id
        else
           flash.now[:alert] = "Payment save failed!"
           render :new
        end                              
    end

    # PATCH/PUT /payments/1
    def update
     # @donor = Donor.find(params[:id]) # so that we can still display the nav menu
      @payment =  Payment.find(params[:id])
        if @payment.update(amount: params[:payment][:amount],
                            paymentType: params[:payment][:paymentType],
                            donor_id: params[:payment][:donor_id],
                            organization_id: params[:payment][:organization_id])
         
            flash[:notice] = "Payment updated successfully!"
            redirect_to :controller => 'donors', :action => 'donor_home', :id => @payment.donor_id
        else
            flash.now[:alert] = "Payment update failed!"
            render :edit
        end             
     end

    # DELETE /payments/1
    def destroy
      begin
           @payment =  Payment.find(params[:id])
            rescue ActiveRecord::RecordNotFound
            flash[:alert] = "Payment destroy failed!"
            redirect_to :controller => 'donors', :action => 'edit_donation', :id => @payment.donor_id and return
      end

        if @payment.destroy
            flash[:notice] = "Payment destroy successfully!"
            redirect_to :controller => 'donors', :action => 'donor_home', :id => @payment.donor_id
        else
            flash[:alert] = "Payment destroy failed!"
            redirect_to :controller => 'donors', :action => 'edit_donation', :id => @payment.donor_id and return
        end       

    end
end
