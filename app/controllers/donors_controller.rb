class DonorsController < ApplicationController
    before_action :set_donor, only: [:edit, :update]

    def donor_home
      @donor = Donor.find(params[:id])
      @tag = DonorTag.find_by_donor_id(@donor.id)
      #render donors/donor_home.html.erb
    end
    
    def new
      @donor = Donor.new
      @all_tags = Tag.all
      @donor_tag = @donor.donor_tags.build
      #render donors/new.html.erb
    end    

    def edit
      @donor = Donor.find(params[:id])
      @all_tags = Tag.all
      @donor_tag = @donor.donor_tags.build
      @selected_tags = @donor.donor_tags  
      #render donors/edit.html.erb
    end

    def profile
      @donor = Donor.find(params[:id])
      #render donors/profile.html.erb
    end


    def edit_donation
      @donor = Donor.find(params[:id])
      #render donors/edit_donation.html.erb
    end
  
     # POST /donors
    def create
      @donor = Donor.new(user: current_user, name: params[:donor][:name],
                       address: params[:donor][:address],
                       dob: params[:donor][:dob])
      
      params[:tags][:id].each do |tag|
            if !tag.empty?
              @donor.donor_tags.build(:tag_id => tag)
            end
        end

        if @donor.save
            flash[:notice] = "Donor profile saved successfully!"
            redirect_to :action => "donor_home", :id => @donor
        else
            @all_tags = Tag.all
            @donor_tag = @donor.donor_tags.build
            flash.now[:alert] = "Donor profile save failed!"
            render :new
        end                              
    end

    #PATCH/PUT /donors/1
    def update
      @donor = Donor.find(params[:id])
      
      @donor.donor_tags.each do |t|
        t.destroy
      end

      params[:tags][:id].each do |tag|
        if !tag.empty?
          @donor.donor_tags.build(:tag_id => tag)
        end
      end                                   
  
        if @donor.update(user: current_user, name: params[:donor][:name],
                         address: params[:donor][:address],
                         dob: params[:donor][:dob])
          
            flash[:notice] = "Donor profile updated successfully!"
            redirect_to :action => "donor_home", :id => @donor
        else
            @all_tags = Tag.all
            @donor_tag = @donor.donor_tags.build
            @selected_tags = @donor.donor_tags
            flash.now[:alert] = "Donor profile update failed!"
            render :edit
        end                          
    end

    private
    # Use callbacks to share common setup or constraints between actions.
    def set_donor
      @donor = Donor.find(params[:id])
    end
    
end
