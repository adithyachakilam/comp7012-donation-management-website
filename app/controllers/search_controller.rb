require 'will_paginate/array'

class SearchController < ApplicationController

	def index
		@tags = Tag.all
		if params[:search]
			search = "%#{params[:search]}%"
		else
			search = "%#{}%"
		end

		@organizations = []
		@t = @tags.where('name like ?', search).first
		if @t != nil and search != "%#{}%"
			@orgWhosTagIsGiven = OrganizationTag.where(tag_id: @t)
			@orgWhosTagIsGiven.each do |org|
				@organizations.push(Organization.find(org.organization.id))
			end
			@organizations = @organizations.paginate(:page => params[:page], :per_page => 2)
		else
			@organizations = Organization.where(
							"name like ? or owner like ? or website like ?
							or address like ? or year like ? or about like ?",
							search, search, search, search, search, search)
			                .paginate(:page => params[:page], :per_page => 2)
		end
	end
end
