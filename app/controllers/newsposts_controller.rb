class NewspostsController < ApplicationController
  before_action :set_newsposts
  before_action :set_newspost, only: [:show, :edit, :update, :destroy]

  # GET organizations/1/newsposts
  def index
    @newsposts = @organization.newsposts
  end

  # GET organizations/1/newsposts/1
  def show
  end

  # GET organizations/1/newsposts/new
  def new
    @newspost = @organization.newsposts.build
  end

  # GET organizations/1/newsposts/1/edit
  def edit
  end

  # POST organizations/1/newsposts
  def create
    @newspost = @organization.newsposts.build(newspost_params)

    if @newspost.save
      redirect_to([@newspost.organization, @newspost], notice: 'Newspost was successfully created.')
    else
      render action: 'new'
    end
  end

  # PUT organizations/1/newsposts/1
  def update
    if @newspost.update_attributes(newspost_params)
      redirect_to([@newspost.organization, @newspost], notice: 'Newspost was successfully updated.')
    else
      render action: 'edit'
    end
  end

  # DELETE organizations/1/newsposts/1
  def destroy
    @newspost.destroy

    redirect_to org_landing_path(@organization)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_newsposts
      @organization = Organization.find(params[:organization_id])
    end

    def set_newspost
      @newspost = @organization.newsposts.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def newspost_params
      params.require(:newspost).permit(:title, :content, :image)
    end
end
