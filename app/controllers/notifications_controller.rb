class NotificationsController < ApplicationController
  before_action :set_notifications
  before_action :set_notification, only: [:show, :edit, :update, :destroy]

  # GET donors/1/notifications
  def index
    @notifications = @donor.notifications
  end

  # GET donors/1/notifications/1
  def show
  end

  # GET donors/1/notifications/new
  def new
    @notification = @donor.notifications.build
  end

  # GET donors/1/notifications/1/edit
  def edit
  end

  # POST donors/1/notifications
  def create
    #@notification = @donor.notifications.build(notification_params)

    Donor.all.each do |d|
      @x= d.notifications.build(notification_params)
      @x.save
    end

    #if @notification.save
      redirect_to(root_path, notice: 'Notification was successfully created.')
    #else
    #  render action: 'new'
    #end
  end

  # PUT donors/1/notifications/1
  def update
    if @notification.update_attributes(notification_params)
      redirect_to([@notification.donor, @notification], notice: 'Notification was successfully updated.')
    else
      render action: 'edit'
    end
  end

  # DELETE donors/1/notifications/1
  def destroy
    @notification.destroy

    redirect_to donor_notifications_url(@donor)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_notifications
      @donor = Donor.find(params[:donor_id])
      if current_user.organization?
        @organization = Organization.where("user_id = #{current_user.id}").take!
      end
    end

    def set_notification
      @notification = @donor.notifications.find(params[:id])

    end

    # Only allow a trusted parameter "white list" through.
    def notification_params
      params.require(:notification).permit(:title, :content)
    end
end
