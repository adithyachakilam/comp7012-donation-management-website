class OrganizationsController < ApplicationController
  before_action :set_organization, only: [:show, :edit, :update, :destroy]

  def landingpage
    @organization = Organization.find(params[:id])
    @newsposts = @organization.newsposts
  end

  def profilepage
    if params.has_key?(:donor)
      @donor = Donor.find(params[:donor]) # to display the nav menu correctly to logged in donor
    end
    @organization = Organization.find(params[:id])
    @newsposts = @organization.newsposts
      
    
  end

  # GET /organizations
  # GET /organizations.json
  def index
    @organizations = Organization.all
  end

  # GET /organizations/1
  # GET /organizations/1.json
  def show
  end

  # GET /organizations/new
  def new
    @organization = Organization.new
    @all_tags = Tag.all
    @organization_tag = @organization.organization_tags.build
    @selected_tags = @organization.organization_tags  
    
  end

  # GET /organizations/1/edit
  def edit
    end

  # POST /organizations
  # POST /organizations.json
  def create
    @organization = Organization.new(organization_params)
    
    params[:tags][:id].each do |tag|
      if !tag.empty?
        @organization.organization_tags.build(:tag_id => tag)
      end
  end

    respond_to do |format|
      if @organization.save
        id= @organization.id
        format.html { redirect_to org_landing_path(id), notice: 'Organization was successfully created.' }
        format.json { render :show, status: :created, location: @organization }
      else
        @all_tags = Tag.all
        @organization_tag = @organization.organization_tags.build
        format.html { render :new }
        format.json { render json: @organization.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /organizations/1
  # PATCH/PUT /organizations/1.json
  def update
    respond_to do |format|
      @organization.organization_tags.each do |t|
        t.destroy
      end

      params[:tags][:id].each do |tag|
        if !tag.empty?
          @organization.organization_tags.build(:tag_id => tag)
        end
      end
      
        if @organization.update(organization_params)
          id= @organization.id
          format.html { redirect_to org_landing_path(id), notice: 'Organization was successfully updated.' }
          format.json { render :show, status: :ok, location: @organization }
        else
          params[:tags][:id].each do |tag|
            if !tag.empty?
              @organization.organization_tags.build(:tag_id => tag)
            end
          end
          @all_tags = Tag.all
          @organization_tag = @organization.organization_tags.build
          @selected_tags = @organization.organization_tags
          format.html { render :edit }
          format.json { render json: @organization.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /organizations/1
  # DELETE /organizations/1.json
  def destroy
    @organization.destroy
    respond_to do |format|
      format.html { redirect_to organizations_url, notice: 'Organization was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_organization
      @organization = Organization.find(params[:id])
      @all_tags = Tag.all
      @organization_tag = @organization.organization_tags.build
      @selected_tags = @organization.organization_tags  
   
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def organization_params
      # Pass in the logged in user
      defaults = { user: current_user }
      params.require(:organization).permit(:user, :name, :owner,
                                           :website, :address,
                                           :year, :about, :id,).reverse_merge(defaults)
    end
end
