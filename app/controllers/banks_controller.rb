class BanksController < ApplicationController
  before_action :set_banks
  before_action :set_bank, only: [:show, :edit, :update, :destroy]

  # GET organizations/1/banks
  def index
    @banks = @organization.banks
  end

  # GET organizations/1/banks/1
  def show
  end

  # GET organizations/1/banks/new
  def new
    @bank = @organization.banks.build
  end

  # GET organizations/1/banks/1/edit
  def edit
  end

  # POST organizations/1/banks
  def create
    @bank = @organization.banks.build(bank_params)

    if @bank.save
      redirect_to([@bank.organization, @bank], notice: 'Bank was successfully created.')
    else
      render action: 'new'
    end
  end

  # PUT organizations/1/banks/1
  def update
    if @bank.update_attributes(bank_params)
      redirect_to([@bank.organization, @bank], notice: 'Bank was successfully updated.')
    else
      render action: 'edit'
    end
  end

  # DELETE organizations/1/banks/1
  def destroy
    @bank.destroy

    redirect_to org_landing_path(@organization)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_banks
      @organization = Organization.find(params[:organization_id])
    end

    def set_bank
      @bank = @organization.banks.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def bank_params
      params.require(:bank).permit(:accountType, :rountingNumber, :accoutnNumber, :accountName)
    end
end
