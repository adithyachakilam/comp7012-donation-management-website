# == Schema Information
#
# Table name: organization_tags
#
#  id              :integer          not null, primary key
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  organization_id :integer
#  tag_id          :integer
#
# Indexes
#
#  index_organization_tags_on_organization_id  (organization_id)
#  index_organization_tags_on_tag_id           (tag_id)
#

class OrganizationTag < ApplicationRecord
    
    belongs_to :organization
    belongs_to :tag
end
