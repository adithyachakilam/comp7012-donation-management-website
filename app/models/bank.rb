# == Schema Information
#
# Table name: banks
#
#  id              :integer          not null, primary key
#  organization_id :integer
#  accountType     :string
#  rountingNumber  :string
#  accoutnNumber   :integer
#  accountName     :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_banks_on_organization_id  (organization_id)
#

class Bank < ApplicationRecord
  belongs_to :organization

  validates :accountType, inclusion: { in: %w(personal private)}, presence: true
  validates :rountingNumber, format: { with: /[A-Z0-9]{9}/ }, presence: true
  validates :accoutnNumber, format: { with: /\d{4,17}/ }, presence: true
  validates :accountName, length: { maximum: 30 }, presence: true

  

end
