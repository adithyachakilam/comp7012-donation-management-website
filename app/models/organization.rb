# == Schema Information
#
# Table name: organizations
#
#  id         :integer          not null, primary key
#  name       :string
#  owner      :string
#  website    :string
#  address    :string
#  year       :integer
#  about      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#
# Indexes
#
#  index_organizations_on_user_id  (user_id)
#


class Organization < ApplicationRecord
  has_many :banks
  belongs_to :user , optional:true
  has_many :newsposts
  has_many :payments
  has_many :organization_tags
  has_many :bank_accounts
  has_many :donors, through: :payments
  has_many :tags, through: :organization_tags



  validates :name, length: { maximum: 40 }, presence: true
  validates :owner, length: { maximum: 30 }, presence: true
  validates :website, format: { with: /https?:\/\/[\S]+/  }, presence: true
  validates :year, numericality: { greater_than_or_equal_to: 1700, less_than_or_equal_to: 2018, allow_nil: true }, allow_nil: true, allow_blank: true
  validate :address_verification

  private
 
  def address_verification
  
    if ( (address != nil) && (StreetAddress::US.parse_address(address) == nil))
      errors.add(:address, "Invalid Format, Use specified")
    end
  end

end
