# == Schema Information
#
# Table name: notifications
#
#  id         :integer          not null, primary key
#  donor_id   :integer
#  title      :string
#  content    :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_notifications_on_donor_id  (donor_id)
#

class Notification < ApplicationRecord
  belongs_to :donor

  validates :title, length: { maximum: 30 }, presence: true
  validates :content, length: { maximum: 120 }, presence: true


end
