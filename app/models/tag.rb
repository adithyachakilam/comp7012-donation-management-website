# == Schema Information
#
# Table name: tags
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Tag < ApplicationRecord

    has_many :donor_tags
    has_many :donors, through: :donor_tags

    has_many :organization_tags
    has_many :organizations, through: :organization_tags

end
