# == Schema Information
#
# Table name: cards
#
#  id          :integer          not null, primary key
#  donor_id    :integer
#  cardNumber  :integer
#  cvv         :integer
#  expiryMonth :integer
#  expiryYear  :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_cards_on_donor_id  (donor_id)
#

class Card < ApplicationRecord
  belongs_to :donor

  validates :cardNumber, presence: true, credit_card_number: true
  validates :cvv, format: { with: /\d{3}/ }, presence: true
  validates :expiryMonth, numericality: { greater_than_or_equal_to: 1, less_than_or_equal_to: 12 }, presence: true
  validates :expiryYear, numericality: { greater_than_or_equal_to: 2018, less_than_or_equal_to: 2030 }, presence: true


end
