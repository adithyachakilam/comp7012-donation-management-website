# == Schema Information
#
# Table name: payments
#
#  id              :integer          not null, primary key
#  amount          :integer
#  paymentType     :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  donor_id        :integer
#  organization_id :integer
#
# Indexes
#
#  index_payments_on_donor_id         (donor_id)
#  index_payments_on_organization_id  (organization_id)
#

class Payment < ApplicationRecord

    belongs_to :donor
    belongs_to :organization

    validates :amount, numericality: { greater_than: 0}, presence: true

end
