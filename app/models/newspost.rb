# == Schema Information
#
# Table name: newsposts
#
#  id              :integer          not null, primary key
#  organization_id :integer
#  title           :string
#  content         :text
#  image           :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_newsposts_on_organization_id  (organization_id)
#

class Newspost < ApplicationRecord
  belongs_to :organization
  mount_uploader :image, ImageUploader

  validates :title, length: { maximum: 40 }, presence: true
  validates :content, length: { minimum: 40 }, presence: true
  #file format for image validation is included in uploader/image_uploader.rb

end
