# == Schema Information
#
# Table name: donors
#
#  id         :integer          not null, primary key
#  name       :string
#  address    :string
#  dob        :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#
# Indexes
#
#  index_donors_on_user_id  (user_id)
#

class Donor < ApplicationRecord

    
    
	belongs_to :user, optional:true
    has_many :payments
    has_many :cards
    has_many :notifications
    has_many :donor_tags
    has_many :organizations, through: :payments
    has_many :tags, through: :donor_tags

    validates :name, length: { maximum: 30 }, presence: true
    validates_format_of :dob, :with => /\d{2}\/\d{2}\/\d{4}/, :message => "Date must be in the following format: mm/dd/yyyy", presence: true
    validate :address_verification


    def address_verification
  
        if ( (address != nil) && (StreetAddress::US.parse_address(address) == nil))
          errors.add(:address, "Invalid Format, Use specified")
        end

    end    
end
