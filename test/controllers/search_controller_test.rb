require 'test_helper'

class SearchControllerTest < ActionDispatch::IntegrationTest
  test "Should not crash on empty search" do
    get search_index_url
    assert_response :success
    assert_template :index
  end

  test "Should not crash on valid tag search" do
  	get search_index_url, params: {search: "Nonprofit Organization"}
  	assert_response :success
    assert_template :index
  end

  test "Should not crash on invalid tag search" do
  	get search_index_url, params: {search: "Nonprofit O"}
  	assert_response :success
    assert_template :index
  end
end
