require 'test_helper'

class OrganizationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @organization = organizations(:one)
  end

  test "should get index" do
    get organizations_url
    assert_response :success
  end

  test "should get new" do
    get new_organization_url
    assert_response :success
  end

  test "should create organization" do
    #assert_difference('Organization.count') do
    #  post organizations_url, params: { organization: { about: @organization.about, address: @organization.address, name: @organization.name, owner: @organization.owner, website: @organization.website, year: @organization.year }
    #end
    #assert_redirected_to org_landing_path((@organization.id) + 1)
  end

  test "should show organization" do
    get organization_url(@organization)
    assert_response :success
  end

  test "should get edit" do
    get edit_organization_url(@organization)
    assert_response :success
  end

  test "should update organization" do
    #patch organization_url(@organization), params: { organization: { about: @organization.about, address: @organization.address, name: @organization.name, owner: @organization.owner, website: @organization.website, year: @organization.year } }
    #assert_redirected_to org_landing_path(@organization)
    get edit_organization_url(@organization)
    assert_response :success
  end

end
