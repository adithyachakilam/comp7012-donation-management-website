require 'test_helper'

class HomeControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  test "Test index when user not signed in" do
    get home_index_url
    assert_response :success
    assert_template :index
  end

  test 'Test donor redirection upon sign in' do
    @donor_user = users(:one)
    # Use the sign_in helper to sign in a fixture `User` record.
    sign_in @donor_user

    get new_donor_url
    assert_response :success
    assert_template :new

    # Associate donor
    @donor_user.donor = donors(:one)

    # Now check that donoe_home page is returned
    get donor_url(@donor_user.donor.id)
    assert_response :success
    assert_template :donor_home
  end

  test 'Test organization redirection upon sign in' do
    @org_user = users(:two)
    # Use the sign_in helper to sign in a fixture `User` record.
    sign_in @org_user

    get new_organization_url
    assert_response :success
    assert_template :new

    # Associate donor
    @org_user.organization = organizations(:one)

    # Now check that donoe_home page is returned
    get org_landing_url(@org_user.organization.id)
    assert_response :success
    assert_template :landingpage
  end

  test 'Behavior for user accidentally not assigned with user type' do
    @anon_user = users(:three)
    sign_in @anon_user

    get home_index_url
    assert_response :success
    assert_template :index
  end
end