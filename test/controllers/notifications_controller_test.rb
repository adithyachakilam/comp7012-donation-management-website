require 'test_helper'

class NotificationsControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers

  setup do
    @donor = donors(:one)
    @notification = notifications(:one)
    @user = users(:one)
    sign_in @user
  end

  test "should get index" do
    get :index, params: { donor_id: @donor }
    assert_response :success
  end

  test "should create notification" do
#    assert_difference('Notification.count') do
      post :create, params: { donor_id: @donor, notification: @notification.attributes }
#    end
    assert_redirected_to root_path()
  end

  test "should destroy notification" do
    assert_difference('Notification.count', -1) do
      delete :destroy, params: { donor_id: @donor, id: @notification }
    end

    assert_redirected_to donor_notifications_path(@donor)
  end
end
