require 'test_helper'

class CardsControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers


  setup do
    @donor = donors(:one)
    @card = cards(:one)
  end

  test "should get index" do
    get :index, params: { donor_id: @donor }
    assert_response :success
  end

  test "should get new" do
    get :new, params: { donor_id: @donor }
    assert_response :success
  end

  test "should create card" do
#    assert_difference('Card.count') do
      post :create, params: { donor_id: @donor, card: @card.attributes }
#    end
    assert_redirected_to donor_card_path(@donor, Card.last)
  end

  test "should show card" do
    get :show, params: { donor_id: @donor, id: @card }
    assert_response :success
  end

  test "should get edit" do
    get :edit, params: { donor_id: @donor, id: @card }
    assert_response :success
  end

  test "should update card" do
    post :update, params: { donor_id: @donor, id: @card, card: @card.attributes }
    assert_redirected_to donor_card_path(@donor,@card)
  end

  test "should destroy card" do
    assert_difference('Card.count', -1) do
      delete :destroy, params: { donor_id: @donor, id: @card }
    end

    assert_redirected_to donor_cards_path(@donor)
  end
end
