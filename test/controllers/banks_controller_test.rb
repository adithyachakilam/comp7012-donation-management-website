require 'test_helper'

class BanksControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers


  setup do
    @organization = organizations(:one)
    @bank = banks(:one)
  end

  test "should get index" do
    get :index, params: { organization_id: @organization }
    assert_response :success
  end

  test "should get new" do
    get :new, params: { organization_id: @organization }
    assert_response :success
  end

  test "should create bank" do
#    assert_difference('Bank.count') do
      post :create, params: { organization_id: @organization, bank: @bank.attributes }
#    end

    assert_redirected_to organization_bank_path(@organization,Bank.last)
  end

  test "should show bank" do
    get :show, params: { organization_id: @organization, id: @bank }
    assert_response :success
  end

  test "should get edit" do
    get :edit, params: { organization_id: @organization, id: @bank }
    assert_response :success
  end

  test "should update bank" do
    put :update, params: { organization_id: @organization, id: @bank, bank: @bank.attributes }
    assert_redirected_to organization_bank_path(@organization,@bank)
  end

  test "should destroy bank" do
    assert_difference('Bank.count', -1) do
      delete :destroy, params: { organization_id: @organization, id: @bank }
    end

    assert_redirected_to org_landing_path(@organization)
  end
end
