require 'test_helper'

class DonorsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @donor = donors(:one)
  end
  
  
  test "should get new donor" do
    get new_donor_url
    assert_response :success
  end

#  test "should create donor" do
#    assert_difference('Donor.count') do
#      post donors_url, params: { donor: { name: @donor.name, address: @donor.address, dob: @donor.dob, donor_tags: @donor.donor_tags.build } }
#    end
#    assert_redirected_to donor_path((@donor.id) + 1)
#  end

  test "should get edit donor" do
    get edit_donor_url(@donor)
    assert_response :success
  end

#  test "should update donor" do
#    patch donor_url(@donor), params: { donor: { name: @donor.name, address: @donor.address, dob: @donor.dob } }
#    assert_redirected_to donor_path(@donor.id)
#end
end
