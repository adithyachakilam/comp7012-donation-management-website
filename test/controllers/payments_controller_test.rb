require 'test_helper'

class PaymentsControllerTest < ActionDispatch::IntegrationTest
 

  setup do
    @donor = donors(:one)
    @organization = organizations(:one)
    @payment = payments(:one)
  end

  test "should get new payment" do
    get new_payment_url(@donor,@organization)
    assert_response :success
  end

  test "should create payment" do
    assert_difference('Payment.count') do
      post payments_url, params: { payment: { amount: @payment.amount, paymentType: @payment.paymentType, donor_id: @payment.donor_id, organization_id: @payment.organization_id } }
    end
    assert_redirected_to donor_path(@payment.donor)
  end

  test "should get edit payment" do
    get edit_payment_url(@payment)
    assert_response :success
  end

  test "should update payment" do
    patch payment_url(@payment), params: { payment: { amount: @payment.amount, paymentType: @payment.paymentType, donor_id: @payment.donor_id, organization_id: @payment.organization_id } }
    assert_redirected_to donor_path(@payment.donor)
  end

  test "should destroy payment" do
    assert_difference('Payment.count', -1) do
      delete payment_url(@payment)
    end
    assert_redirected_to donor_path(@payment.donor)
    
  end


end
