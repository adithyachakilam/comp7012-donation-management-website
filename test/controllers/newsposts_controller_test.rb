require 'test_helper'

class NewspostsControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers

  setup do
    @organization = organizations(:one)
    @newspost = newsposts(:one)
  end

  test "should get index" do
    get :index, params: { organization_id: @organization }
    assert_response :success
  end

  test "should get new" do
    get :new, params: { organization_id: @organization }
    assert_response :success
  end

  test "should create newspost" do
  #   assert_difference('Newspost.count') do
      post :create, params: { organization_id: @organization, newspost: @newspost.attributes }
  # end
  assert_redirected_to organization_newspost_path(@organization, Newspost.last)
  end

  test "should show newspost" do
    get :show, params: { organization_id: @organization, id: @newspost }
    assert_response :success
  end

  test "should get edit" do
    get :edit, params: { organization_id: @organization, id: @newspost }
    assert_response :success
  end

  test "should update newspost" do
    put :update, params: { organization_id: @organization, id: @newspost, newspost: @newspost.attributes }
    assert_redirected_to organization_newspost_path(@organization, Newspost.last)
  end

  test "should destroy newspost" do
    assert_difference('Newspost.count', -1) do
      delete :destroy, params: { organization_id: @organization, id: @newspost }
    end

    assert_redirected_to org_landing_path(@organization)
  end
end
