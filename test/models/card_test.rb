# == Schema Information
#
# Table name: cards
#
#  id          :integer          not null, primary key
#  donor_id    :integer
#  cardNumber  :integer
#  cvv         :integer
#  expiryMonth :integer
#  expiryYear  :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_cards_on_donor_id  (donor_id)
#

require 'test_helper'

class CardTest < ActiveSupport::TestCase


  test "Valid Card " do
    crd = cards(:one)
    assert crd.valid?  
  end

  test "card NUmber must be present" do
    crd = cards(:one)
    crd.cardNumber = nil
    assert_not crd.valid?  
  end

  test "card NUmber - invalid format" do
    crd = cards(:one)
    crd.cardNumber = 123456789
    assert_not crd.valid?  
  end

  test "cvv NUmber must be present" do
    crd = cards(:one)
    crd.cvv = nil
    assert_not crd.valid?  
  end

  test "cvv must be 3" do
    crd = cards(:one)
    crd.cvv = 1
    assert_not crd.valid?  
  end

  test "Expiry month must be present" do
    crd = cards(:one)
    crd.expiryMonth = nil
    assert_not crd.valid?  
  end

  test "Expiry month must be between 1-12" do
    crd = cards(:one)
    crd.expiryMonth = 14
    assert_not crd.valid?  
  end

  test "expiry year must be present" do
    crd = cards(:one)
    crd.expiryYear = nil
    assert_not crd.valid?  
  end

  test "expiry year must be greater that oor equal to 2018" do
    crd = cards(:one)
    crd.expiryYear = 2017
    assert_not crd.valid?  
  end

end
