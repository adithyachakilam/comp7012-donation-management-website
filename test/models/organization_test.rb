# == Schema Information
#
# Table name: organizations
#
#  id         :integer          not null, primary key
#  name       :string
#  owner      :string
#  website    :string
#  address    :string
#  year       :integer
#  about      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#
# Indexes
#
#  index_organizations_on_user_id  (user_id)
#


require 'test_helper'

class OrganizationTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end


  test "Name should be less than 40 characters " do
    org = organizations(:one)
    org.name = "Tommmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm"
    assert_not org.valid?  
  end

  test "Owner should be less than 40 characters " do
    org = organizations(:one)
    org.owner = "Tommmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm"
    assert_not org.valid?  
  end

  test "Should Be Valid Website " do
    org = organizations(:one)
    org.website= "google.com"
    assert_not org.valid?  
  end

  test "Year should be greater than 1700 " do
    org = organizations(:one)
    org.year= 1600
    assert_not org.valid?  
  end

  test "Year should be lesser than 2019 " do
    org = organizations(:one)
    org.year= 2020
    assert_not org.valid?  
  end

  test "Valid Year" do
    org = organizations(:one)
    org.year= nil
    assert org.valid?
  end

  
end
