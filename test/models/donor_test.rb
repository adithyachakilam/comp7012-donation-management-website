# == Schema Information
#
# Table name: donors
#
#  id         :integer          not null, primary key
#  name       :string
#  address    :string
#  dob        :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#
# Indexes
#
#  index_donors_on_user_id  (user_id)
#

require 'test_helper'

class DonorTest < ActiveSupport::TestCase
  
  test "Name should be less than 30 characters " do
    dnr = donors(:one)
    dnr.name = "TestTestTestTestTestTestTestTest"
    assert_not dnr.valid?  
  end

  test "Name should not be blank " do
    dnr = donors(:one)
    dnr.name = nil
    assert_not dnr.valid?  
  end

  test "Date should not be blank " do
    dnr = donors(:one)
    dnr.dob = nil
    assert_not dnr.valid?  
  end

  test "Date Should be in mm/DD/YYYY format " do
    dnr = donors(:one)
    dnr.dob = "2014/04/04"
    assert_not dnr.valid?  
  end
end
