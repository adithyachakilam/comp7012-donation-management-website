# == Schema Information
#
# Table name: notifications
#
#  id         :integer          not null, primary key
#  donor_id   :integer
#  title      :string
#  content    :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_notifications_on_donor_id  (donor_id)
#

require 'test_helper'

class NotificationTest < ActiveSupport::TestCase
  
  test "Valid Notification" do
    one= notifications(:one)
    assert one.valid?
  end

  test "Title must be present" do
    one= notifications(:one)
    one.title= nil
    assert_not one.valid?
  end

  test "Title must be less than 30 chars" do
    one= notifications(:one)
    one.title= "EXAMPLEEXAMPLEEXAMPLEEXAMPLEEXAMPLEEXAMPLEEXAMPLEEXAMPLE"
    assert_not one.valid?
  end

  test "Content must be present" do
    one= notifications(:one)
    one.content= nil
    assert_not one.valid?
  end

  test "Content must be less than 120 chars" do
    one= notifications(:one)
    one.content= "EXAMPLEEXAMPLEEXAMPLEEXAMPLEEXAMPLEEXAMPLEEXAMPLEEXAMPLE
    EXAMPLEEXAMPLEEXAMPLEEXAMPLEEXAMPLEEXAMPLEEXAMPLEEXAMPLE
    EXAMPLEEXAMPLEEXAMPLEEXAMPLEEXAMPLEEXAMPLEEXAMPLEEXAMPLE
    EXAMPLEEXAMPLEEXAMPLEEXAMPLEEXAMPLEEXAMPLEEXAMPLEEXAMPLE
    EXAMPLEEXAMPLEEXAMPLEEXAMPLEEXAMPLEEXAMPLEEXAMPLEEXAMPLE
    EXAMPLEEXAMPLEEXAMPLEEXAMPLEEXAMPLEEXAMPLEEXAMPLEEXAMPLE"
    assert_not one.valid?
  end

end
