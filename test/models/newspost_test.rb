# == Schema Information
#
# Table name: newsposts
#
#  id              :integer          not null, primary key
#  organization_id :integer
#  title           :string
#  content         :text
#  image           :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_newsposts_on_organization_id  (organization_id)
#

require 'test_helper'

class NewspostTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end


  test "Valid NewsPost" do
    one= newsposts(:one)
    assert one.valid?
  end

  test "Less Content length" do
    one= newsposts(:one)
    one.content = "xxxxxx"
    assert_not one.valid?
  end

  test "Title length exceded" do
    one= newsposts(:one)
    one.title = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
    assert_not one.valid?
  end

  #file format for image validation is included in uploader/image_uploader.rb

end
