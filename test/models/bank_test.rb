# == Schema Information
#
# Table name: banks
#
#  id              :integer          not null, primary key
#  organization_id :integer
#  accountType     :string
#  rountingNumber  :string
#  accoutnNumber   :integer
#  accountName     :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_banks_on_organization_id  (organization_id)
#

require 'test_helper'

class BankTest < ActiveSupport::TestCase
  
  test "Valid Bank " do
    bank = banks(:one)
    assert bank.valid?  
  end

  test "Type must be present " do
    bank = banks(:one)
    bank.accountType = nil
    assert_not bank.valid?  
  end

  test "routing number must be present " do
    bank = banks(:one)
    bank.rountingNumber = nil
    assert_not bank.valid?  
  end

  test "routing number must be 9 alphanumeric characters " do
    bank = banks(:one)
    bank.rountingNumber = "ASDFG1"
    assert_not bank.valid?  
  end

  test "Account Number must be present " do
    bank = banks(:one)
    bank.accoutnNumber = nil
    assert_not bank.valid?  
  end

  test "Account Number must be betweeen 4-17 digints " do
    bank = banks(:one)
    bank.accoutnNumber = 123
    assert_not bank.valid?  
  end

  test "Account Name must be present " do
    bank = banks(:one)
    bank.accountName = nil
    assert_not bank.valid?  
  end

  test "Account Name must be less than 30 characters " do
    bank = banks(:one)
    bank.accountName = "EXAMPLEEXAMPLEEXAMPLEEXAMPLEEXAMPLEEXAMPLEEXAMPLEEXAMPLEEXAMPLEEXAMPLEEXAMPLEEXAMPLE"
    assert_not bank.valid?  
  end


end
