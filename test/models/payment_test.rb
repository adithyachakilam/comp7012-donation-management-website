# == Schema Information
#
# Table name: payments
#
#  id              :integer          not null, primary key
#  amount          :integer
#  paymentType     :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  donor_id        :integer
#  organization_id :integer
#
# Indexes
#
#  index_payments_on_donor_id         (donor_id)
#  index_payments_on_organization_id  (organization_id)
#

require 'test_helper'

class PaymentTest < ActiveSupport::TestCase

  test "Amount should not be blank " do
    pay = payments(:one)
    pay.amount = nil
    assert_not pay.valid?  
  end

  test "Amount should be valid positive " do
    pay = payments(:one)
    pay.amount = -1
    assert_not pay.valid?  
  end
  
end
