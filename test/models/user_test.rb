# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string
#  last_sign_in_ip        :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  role                   :integer
#
# Indexes
#
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#


require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test "User validity" do
    user = users(:one)
    assert user.valid?

    user.email = ""
    assert_not user.valid?

    user.email = "test1@example.com"

    begin
      # Throws argument error as there is no such role
      user.role = :test
      # Make sure that if somehow above statement passes we throw an error
      assert false
    rescue
      # User is still valid as above try fails
      assert user.valid?
    end

    user.role = :organization
    assert user.valid?

    user = users(:three)
    assert_not user.valid?

    user.role = :donor
    assert user.valid?
  end
end
