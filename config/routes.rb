Rails.application.routes.draw do

  get "organizations/landing/:id", to: "organizations#landingpage", as: "org_landing"
  get "organizations/profile/:id", to: "organizations#profilepage", as: "org_profile"
  resources :organizations do
    resources :newsposts
    resources :banks
  end

  get 'home/index'
  get 'search/index'

  devise_for :users, controllers: {
    sessions: 'users/sessions',
    registrations: 'users/registrations'
  }
  
  get 'donors/new', to: 'donors#new', as: 'new_donor'
  post 'donors', to: 'donors#create'
  get 'donors/:id', to: 'donors#donor_home', as: 'donor'
  get 'donors/:id/profile', to: 'donors#profile', as: 'profile_donor'
  get 'donors/:id/edit', to: 'donors#edit', as: 'edit_donor'
  get 'donors/:id/edit_donation', to: 'donors#edit_donation', as: 'edit_donation'
  put 'donors/:id', to: 'donors#update'
  patch 'donors/:id', to: 'donors#update'
  
  resources :donors do
    resources :notifications
    resources :cards
  end
  

  get 'payments/:id/edit', to: 'payments#edit', as: 'edit_payment'
  get 'payments/:id/donor/:donorid/new', to: 'payments#new', as: 'new_payment'
  post 'payments', to: 'payments#create'
  get 'payments/:id', to: 'payments#show', as: 'payment'
  put 'payments/:id', to: 'payments#update'
  patch 'payments/:id', to: 'payments#update'
  delete 'payments/:id', to: 'payments#destroy'

  
  root to: "home#index"

end
