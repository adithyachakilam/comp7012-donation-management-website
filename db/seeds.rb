# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

test1 = User.new(email: 'testd1@mail.com', password: '123456', role: :donor)
test2 = User.new(email: 'testo2@mail.com', password: '154789', role: :organization)
test3 = User.new(email: 'testo3@mail.com', password: '154789', role: :organization)
test4 = User.new(email: 'testo4@mail.com', password: '154789', role: :organization)
test5 = User.new(email: 'testo5@mail.com', password: '154789', role: :organization)
test8 = User.new(email: 'testo6@mail.com', password: '154789', role: :organization)
test9 = User.new(email: 'testo7@mail.com', password: '154789', role: :organization)
test6 = User.new(email: 'testd6@mail.com', password: '154789', role: :donor)
test7 = User.new(email: 'testd7@mail.com', password: '154789', role: :donor)

dnr1 = Donor.new(name: 'Sky', address: '1600 Pennsylvania Ave, Washington, DC, 20500', dob: '11/11/1980')
dnr2 = Donor.new(name: 'Sam', address: '5905 Richmond Hwy Ste 341 Alexandria VA 22304-1864', dob: '05/04/1945')
dnr3 = Donor.new(name: 'Michael', address: '419 Park Hwy, Kansas, TX, 22403', dob: '06/11/1992')

org1 = Organization.new(name: 'Make A Wish', owner: 'David', website: 'http://makeawish.com', address: '5904 Richmond Hwy Ste 340 Alexandria VA 22303-1864', year: 1980, about: 'Public organization which makes the wishes of poor people possible')
org2 = Organization.new(name: 'American RedCross', owner: 'McGovern', website: 'https://redcross.org', address: '605 Patterson Street, Memphis, VA, 22303', year: 1880, about: 'A humanitarian organization that provides emergency assistance, disaster relief and education in the United States')
org3 = Organization.new(name: 'GoodWill', owner: 'Vikram Patel', website: 'http://goodwill.com', address: '25 West Street, Russelville, MO, 22303', year: 1900, about: 'Good Will organization')
org4 = Organization.new(name: 'Global Giving', owner: 'Mari Kurashi', website: 'http://globalgiving.com', address: '85 Spotswood Ave, LittleRock, AR, 78201', year: 1960, about: 'GlobalGiving is 501 non-profit organization based in the United States that provides a global crowdfunding platform for grassroots charitable projects.')
org5 = Organization.new(name: 'Donate Life America', owner: 'Justin Dornig', website: 'http://donatelife.net', address: '409 Park Hwy, Kansas, TX, 22303', year: 1992, about: 'Donate Life America, founded in 1992 as the Coalition on Donation, is a 5013 not-for-profit alliance of national organizations')
org6 = Organization.new(name: 'The Salvation Army', owner: 'William Booth', website: 'http://salvationarmy.org', address: '622 Brister Street, Memphis, TN, 38111', year: 1865, about: 'The Salvation Army is a Protestant Christian para-church and an international charitable organisation structured in a quasi-military fashion')


post1 = org1.newsposts.new(title:'Encouraging People', content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam at nisl vehicula, laoreet nisi sed, auctor felis. Praesent cursus volutpat leo, eget tincidunt mi volutpat non. Integer laoreet urna ut nulla accumsan, ac posuere dolor ultricies. Maecenas eget ultrices elit. Cras tincidunt lorem at justo semper, quis vulputate arcu lacinia. Morbi et auctor nunc. Proin et nisl accumsan tellus varius varius. Nam efficitur sagittis nibh, non semper dui ultricies et. Morbi facilisis efficitur bibendum. Nullam ornare metus ac neque imperdiet sagittis. Aliquam volutpat maximus vehicula. Donec finibus erat eu dictum elementum.', image: '/app/assets/images/test.png')
post2 = org1.newsposts.new(title:'Creating Doniations', content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam at nisl vehicula, laoreet nisi sed, auctor felis. Praesent cursus volutpat leo, eget tincidunt mi volutpat non. Integer laoreet urna ut nulla accumsan, ac posuere dolor ultricies. Maecenas eget ultrices elit. Cras tincidunt lorem at justo semper, quis vulputate arcu lacinia. Morbi et auctor nunc. Proin et nisl accumsan tellus varius varius. Nam efficitur sagittis nibh, non semper dui ultricies et. Morbi facilisis efficitur bibendum. Nullam ornare metus ac neque imperdiet sagittis. Aliquam volutpat maximus vehicula. Donec finibus erat eu dictum elementum.', image: '/app/assets/images/test.png')
post3 = org2.newsposts.new(title:'Come Foward to Donate', content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam at nisl vehicula, laoreet nisi sed, auctor felis. Praesent cursus volutpat leo, eget tincidunt mi volutpat non. Integer laoreet urna ut nulla accumsan, ac posuere dolor ultricies. Maecenas eget ultrices elit. Cras tincidunt lorem at justo semper, quis vulputate arcu lacinia. Morbi et auctor nunc. Proin et nisl accumsan tellus varius varius. Nam efficitur sagittis nibh, non semper dui ultricies et. Morbi facilisis efficitur bibendum. Nullam ornare metus ac neque imperdiet sagittis. Aliquam volutpat maximus vehicula. Donec finibus erat eu dictum elementum.', image: '/app/assets/images/test.png')
post4 = org2.newsposts.new(title:'American Challenges', content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam at nisl vehicula, laoreet nisi sed, auctor felis. Praesent cursus volutpat leo, eget tincidunt mi volutpat non. Integer laoreet urna ut nulla accumsan, ac posuere dolor ultricies. Maecenas eget ultrices elit. Cras tincidunt lorem at justo semper, quis vulputate arcu lacinia. Morbi et auctor nunc. Proin et nisl accumsan tellus varius varius. Nam efficitur sagittis nibh, non semper dui ultricies et. Morbi facilisis efficitur bibendum. Nullam ornare metus ac neque imperdiet sagittis. Aliquam volutpat maximus vehicula. Donec finibus erat eu dictum elementum.', image: '/app/assets/images/test.png')
post5 = org2.newsposts.new(title:'Gving Spirit', content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam at nisl vehicula, laoreet nisi sed, auctor felis. Praesent cursus volutpat leo, eget tincidunt mi volutpat non. Integer laoreet urna ut nulla accumsan, ac posuere dolor ultricies. Maecenas eget ultrices elit. Cras tincidunt lorem at justo semper, quis vulputate arcu lacinia. Morbi et auctor nunc. Proin et nisl accumsan tellus varius varius. Nam efficitur sagittis nibh, non semper dui ultricies et. Morbi facilisis efficitur bibendum. Nullam ornare metus ac neque imperdiet sagittis. Aliquam volutpat maximus vehicula. Donec finibus erat eu dictum elementum.', image: '/app/assets/images/test.png')
post6 = org3.newsposts.new(title:'Charity Navigator', content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam at nisl vehicula, laoreet nisi sed, auctor felis. Praesent cursus volutpat leo, eget tincidunt mi volutpat non. Integer laoreet urna ut nulla accumsan, ac posuere dolor ultricies. Maecenas eget ultrices elit. Cras tincidunt lorem at justo semper, quis vulputate arcu lacinia. Morbi et auctor nunc. Proin et nisl accumsan tellus varius varius. Nam efficitur sagittis nibh, non semper dui ultricies et. Morbi facilisis efficitur bibendum. Nullam ornare metus ac neque imperdiet sagittis. Aliquam volutpat maximus vehicula. Donec finibus erat eu dictum elementum.')
post7 = org3.newsposts.new(title:'Network For Good', content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam at nisl vehicula, laoreet nisi sed, auctor felis. Praesent cursus volutpat leo, eget tincidunt mi volutpat non. Integer laoreet urna ut nulla accumsan, ac posuere dolor ultricies. Maecenas eget ultrices elit. Cras tincidunt lorem at justo semper, quis vulputate arcu lacinia. Morbi et auctor nunc. Proin et nisl accumsan tellus varius varius. Nam efficitur sagittis nibh, non semper dui ultricies et. Morbi facilisis efficitur bibendum. Nullam ornare metus ac neque imperdiet sagittis. Aliquam volutpat maximus vehicula. Donec finibus erat eu dictum elementum.')
post8 = org4.newsposts.new(title:'All about Donating', content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam at nisl vehicula, laoreet nisi sed, auctor felis. Praesent cursus volutpat leo, eget tincidunt mi volutpat non. Integer laoreet urna ut nulla accumsan, ac posuere dolor ultricies. Maecenas eget ultrices elit. Cras tincidunt lorem at justo semper, quis vulputate arcu lacinia. Morbi et auctor nunc. Proin et nisl accumsan tellus varius varius. Nam efficitur sagittis nibh, non semper dui ultricies et. Morbi facilisis efficitur bibendum. Nullam ornare metus ac neque imperdiet sagittis. Aliquam volutpat maximus vehicula. Donec finibus erat eu dictum elementum.')


bank1 = org1.banks.new(accountType:'personal', rountingNumber: 'ABCGNA125',accoutnNumber: 123589625,accountName: 'Adithya')
bank1 = org2.banks.new(accountType:'personal', rountingNumber: 'BNCH125SC',accoutnNumber: 4589632,accountName: 'Akash')
bank1 = org3.banks.new(accountType:'private', rountingNumber: 'TYH768CG4',accoutnNumber: 12547852369,accountName: 'Thomas')
bank1 = org4.banks.new(accountType:'personal', rountingNumber: '125369857',accoutnNumber: 125849632,accountName: 'austin')
bank1 = org5.banks.new(accountType:'private', rountingNumber: 'ZXCVBNMJH',accoutnNumber: 745874569,accountName: 'Smith')
bank1 = org6.banks.new(accountType:'personal', rountingNumber: 'ABCGNA125',accoutnNumber: 65896569,accountName: 'Person')

card1 = dnr1.cards.new(cardNumber: 4400665819299239, cvv: 234, expiryMonth: 12, expiryYear: 2023)
card1 = dnr2.cards.new(cardNumber: 4400665819299239, cvv: 445, expiryMonth: 05, expiryYear: 2022)
card1 = dnr3.cards.new(cardNumber: 4400665819299239, cvv: 789, expiryMonth: 11, expiryYear: 2021)

tag1 = org1.tags.new(name: 'Nonprofit Organization')
tag2 = org2.tags.new(name: 'Charitable Organization')
tag3 = org3.tags.new(name: 'Cooperative')
tag4 = org4.tags.new(name: 'Alumni Association')
tag5 = org5.tags.new(name: 'Voluntary Association')
tag6 = org6.tags.new(name: 'Professional Association')

tag1.save!
tag2.save!
tag3.save!
tag4.save!
tag5.save!
tag6.save!

post1.save!
post2.save!
post3.save!
post4.save!
post5.save!
post6.save!
post7.save!
post8.save!

dnr1.user = test1
dnr2.user = test6
dnr3.user = test7


org1.user = test2
org2.user = test3
org3.user = test4
org4.user = test5
org5.user = test8
org6.user = test9


dnr1.save!
dnr2.save!
dnr3.save!

org1.save!
org2.save!
org3.save!
org4.save!
org5.save!
org6.save!


test1.save!
test2.save!
test3.save!
test4.save!
test5.save!
test6.save!
test7.save!
