class CreateOrganizationTags < ActiveRecord::Migration[5.1]
  def change
    create_table :organization_tags do |t|

      t.timestamps
    end
  end
end
