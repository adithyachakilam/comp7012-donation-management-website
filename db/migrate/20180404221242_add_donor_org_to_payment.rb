class AddDonorOrgToPayment < ActiveRecord::Migration[5.1]
  def change
    add_reference :payments, :donor, foreign_key: true
    add_reference :payments, :organization, foreign_key: true
  end
end
