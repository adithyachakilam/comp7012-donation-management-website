class ChangeTypeInBanks < ActiveRecord::Migration[5.1]
  def change
    change_column :banks, :rountingNumber, :string
  end
end
