class AddDonorTagToDonortags < ActiveRecord::Migration[5.1]
  def change
    add_reference :donor_tags, :donor, foreign_key: true
    add_reference :donor_tags, :tag, foreign_key: true
  end
end
