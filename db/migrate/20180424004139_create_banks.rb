class CreateBanks < ActiveRecord::Migration[5.1]
  def change
    create_table :banks do |t|
      t.references :organization, foreign_key: true
      t.string :accountType
      t.integer :rountingNumber
      t.integer :accoutnNumber
      t.string :accountName

      t.timestamps
    end
  end
end
