class CreateCards < ActiveRecord::Migration[5.1]
  def change
    create_table :cards do |t|
      t.references :donor, foreign_key: true
      t.integer :cardNumber
      t.integer :cvv
      t.integer :expiryMonth
      t.integer :expiryYear

      t.timestamps
    end
  end
end
