class AddOrganizationTagToOrganizationtags < ActiveRecord::Migration[5.1]
  def change
    add_reference :organization_tags, :organization, foreign_key: true
    add_reference :organization_tags, :tag, foreign_key: true
  end
  
end
