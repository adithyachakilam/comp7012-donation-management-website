class CreateNewsposts < ActiveRecord::Migration[5.1]
  def change
    create_table :newsposts do |t|
      t.references :organization, foreign_key: true
      t.string :title
      t.text :content
      t.string :image

      t.timestamps
    end
  end
end
