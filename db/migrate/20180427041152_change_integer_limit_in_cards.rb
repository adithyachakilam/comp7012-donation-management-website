class ChangeIntegerLimitInCards < ActiveRecord::Migration[5.1]
  def change
    change_column :cards, :cardNumber, :integer, limit: 8
  end
end
