class CreateDonorTags < ActiveRecord::Migration[5.1]
  def change
    create_table :donor_tags do |t|

      t.timestamps
    end
  end
end
