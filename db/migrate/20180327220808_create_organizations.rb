class CreateOrganizations < ActiveRecord::Migration[5.1]
  def change
    create_table :organizations do |t|
      t.string :name
      t.string :owner
      t.string :website
      t.string :address
      t.integer :year
      t.string :about

      t.timestamps
    end
  end
end
