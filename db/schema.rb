# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180501163259) do

  create_table "banks", force: :cascade do |t|
    t.integer "organization_id"
    t.string "accountType"
    t.string "rountingNumber"
    t.integer "accoutnNumber", limit: 8
    t.string "accountName"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["organization_id"], name: "index_banks_on_organization_id"
  end

  create_table "cards", force: :cascade do |t|
    t.integer "donor_id"
    t.integer "cardNumber", limit: 8
    t.integer "cvv"
    t.integer "expiryMonth"
    t.integer "expiryYear"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["donor_id"], name: "index_cards_on_donor_id"
  end

  create_table "donor_tags", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "donor_id"
    t.integer "tag_id"
    t.index ["donor_id"], name: "index_donor_tags_on_donor_id"
    t.index ["tag_id"], name: "index_donor_tags_on_tag_id"
  end

  create_table "donors", force: :cascade do |t|
    t.string "name"
    t.string "address"
    t.string "dob"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "user_id"
    t.index ["user_id"], name: "index_donors_on_user_id"
  end

  create_table "newsposts", force: :cascade do |t|
    t.integer "organization_id"
    t.string "title"
    t.text "content"
    t.string "image"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["organization_id"], name: "index_newsposts_on_organization_id"
  end

  create_table "notifications", force: :cascade do |t|
    t.integer "donor_id"
    t.string "title"
    t.text "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["donor_id"], name: "index_notifications_on_donor_id"
  end

  create_table "organization_tags", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "organization_id"
    t.integer "tag_id"
    t.index ["organization_id"], name: "index_organization_tags_on_organization_id"
    t.index ["tag_id"], name: "index_organization_tags_on_tag_id"
  end

  create_table "organizations", force: :cascade do |t|
    t.string "name"
    t.string "owner"
    t.string "website"
    t.string "address"
    t.integer "year"
    t.string "about"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "user_id"
    t.index ["user_id"], name: "index_organizations_on_user_id"
  end

  create_table "payments", force: :cascade do |t|
    t.integer "amount"
    t.string "paymentType"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "donor_id"
    t.integer "organization_id"
    t.index ["donor_id"], name: "index_payments_on_donor_id"
    t.index ["organization_id"], name: "index_payments_on_organization_id"
  end

  create_table "tags", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "role"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
