# Donation Management Website

## Dependencies

* Image Magic:

    sudo apt install imagemagick

* annotate
* bootstrap-datepicker-rails
* carrierwave
* devise
* dotenv-rails
* jquery-rails
* mini_magick
* nested_scaffold
* rails-controller-testing
* recaptcha
* StreetAddress
* tzinfo-data
* will_paginate

## Setting up the project

### Clone the repository

     git clone --depth 1 https://github.com/memphis-cs-projects/comp7012-Donation-Management-Website

Don't use --depth 1 if you plan to checkout any branch.

### Install

     sudo apt install imagemagick

     cd comp7012-Donation-Management-Website

     gem install sprockets-rails

     bundle install

### Database

     rails db:migrate

     rails db:reset

### Running

     rails s -b 0.0.0.0 -d

-d runs it in background.

Point your browser to localhost:3000

Note that recaptcha will not work for any domain other than localhost. You will need to add keys
for your domain to ``.env``
