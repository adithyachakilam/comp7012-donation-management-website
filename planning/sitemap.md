# SiteMap

**Team:**  Donation Management Website

- #### Homepage /home 
  - List Of Organizations: each organization is clickable and clicking to the orgazination will redirect to its profile page.
  - UserLogin: will redirect to donorHomepage or organizationHomepage based on the userType selected by user at registration time.

- #### Registration /registrations
  - Register: will redirect to Homepage

- #### DonorHomepage /donor_userpage/:id
  - List Of Organizations: show the list of organizations that populated randomly on  DonorUserpage where user click the organization will redirect to the org_profile page.
  - List Of Donations: show the list of organizations that user donate amount.
  - Notifications: shows the number of notifications genereated by the selected organization.
  - EditDonorprofile: will redirect to donor_edit_profile page.
  - EditDonation: will redirect to donor_edit_donation page.

- #### OrganizationHomePage /org_userpage/:id
  - List of Donors: show the list of donors who donate the organization and total amount that each organization has.
  - EditOrganizationProfile: will redirect to org_edit_profile page.
  - CreateNewPost: will redirect to org_new_post page.      
  - SendNotificationsToDonors: will redirect to org_send_notification page.

- #### OrganizationProfile /org_profile/:id
  - OrgProfile: conatins the details of the organization and donate the organization will redirect to donor_create_donation page.

- #### EditDonorprofile /donor_userpage/donor_edit_profile/:id
  - UpdateDetails: update the personal information about donors and also select the tags that donor intersets for particular organization and this will redirect to donor_userpage with the updated organization that donor suggests.

- #### EditDonation /donor_userpage/donor_edit_donation/:id
  - List of donations: show the list of organizers whom the donor donate with details.
  - EditDonationDetails: will redirect to donor_create_donation page.
  - DeleteDonationDetails: will redirect to donor_userpage.

- #### Create/Update Donation /donor_userpage/donor_edit_donation:/id/donor_create_donation
  - UpdateDonation: will update the donationdetails for particular organization and will redirect to donor_userpage.
  - CreateDonation: will create the new donation for organization and will redirect to donor_userpage.

- #### EditOrganizationprofile /org_userpage/org_edit_profile/:id
  - UpdateOrgDetails: update the general information of the organization and redirect to org_userpage.
  - EditTags: edit the tag of particular organization that will redirect to org_edit_tags page.

- #### CreateNewpost /org_userpage/org_new_post/:id
  - Newpost: will redirect to the org_profile page.

- #### SendNotificationtodonors /org_userpage/org_send_notification/:id
  - SendNotification: organization send notification for specific tags that will redirect to org_userpage.

- #### EditTags /org_userpage/org_edit_profile/org_edit_tags/:id
  - EditTags: this page will update the tag information as well as delete the tags from its profile and then this page will redirect to org_userpage. 
