# Milestone 1 - Instructions to the Instructors

- Team: Donation Management Website
- GitHub Repo URL: https://github.com/memphis-cs-projects/comp7012-Donation-Management-Website
- Demo Video URL: https://youtu.be/pClpAgRPiok
- Git Tag for Demo Video Version of Code: milestone1

## File Locations in Repository

- Milestone 1 Individual Assignment Outcomes:  [planning/assignments/assignments-1.md](../assignments/assignments-1.md)
- Who-Did-What Document for Demo Video: [planning/assignments/milestone1-who-did-what.md](../assignments/milestone1-who-did-what.md)
- Up-to-Date Requirements and Design Artifacts:
  - User stories: [planning/user_stories.md](../user_stories.md)
  - Sitemap: [planning/sitemap.md](../sitemap.md)
  - Class diagram of your model: [planning/class_diagrams/class_associations.pdf](../class_diagrams/class_associations.pdf)
