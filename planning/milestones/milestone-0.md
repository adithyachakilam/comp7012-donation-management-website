# Milestone 0 - Instructions to the Instructors

- Team: Donation Management Website
- GitHub Repo URL: https://github.com/memphis-cs-projects/comp7012-Donation-Management-Website

## File Locations in Repository

<!--We specify the file name in this format so that GitHub can resolve the links)-->
- User stories: [planning/user_stories.md](../user_stories.md)
- Sitemap: https: [planning/sitemap.md](../sitemap.md)
- User interface designs: [planning/interface_designs](../interface_designs)
- Class diagram of your model: [planning/class_diagrams/class_associations.pdf](../class_diagrams/class_associations.pdf)
- Milestone 0 individual assignments and outcomes: [planning/assignments/assignments-0.md](../assignments/assignments-0.md)