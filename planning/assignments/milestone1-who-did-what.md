# Demo: Milestone-1 DonationManagement Website

## Video: <https://youtu.be/pClpAgRPiok>

### Goal: User management ([0:43-5:32](https://youtu.be/pClpAgRPiok?t=43s))

- Done by: agawande
- Description: Overriding devise views and controllers whereever necessary. Added role, validation and unit tests for role, Added recapthca, home page, home controller, and unit tests.

### Goal: Organization profile & newspost ([5:34-7:59](https://youtu.be/pClpAgRPiok?t=5m34s))

- Done by: chakilam
- Description: Create organization and newpost model, views, controllers and apply validation and unit tests for each and association between Organizations and Payments by has_many and through.

### Goal: Donor profile & payment ([8:02-9:47](https://youtu.be/pClpAgRPiok?t=8m02s))

- Done by: ashah1
- Description: Create donor and payment model, views, controllers and apply validation and unit tests for each.
