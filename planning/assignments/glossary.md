# Glossary of Terms

**Team:** Donation Management Website

- Web App
  - Refers to this project of Donation Management Website.

- User
  - A User is defined as a generic term that can refer to either Donor or Organization.

- Donor
  - The User that can donate funds to Organization.

- Organization
  - The User that can accept funds from Donors.

- Tags
  - A term which is the specific area towards which user are inclined.
