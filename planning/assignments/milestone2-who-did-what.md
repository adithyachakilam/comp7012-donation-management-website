# Demo: Milestone-2 DonationManagement Website

## Video: <https://youtu.be/-OZMbSu6uRU>

### Goal: UserInterface and Search Organizations ([0:01-2:34](https://youtu.be/-OZMbSu6uRU?t=01s))

- Done by: agawande
- Description: Implement bootstrap setup and provide UI to all pages of website. Added search organization feature, edited home page, search controller, and unit tests.

### Goal: BankDetail,Card  & notification ([6:50-9:55](https://youtu.be/-OZMbSu6uRU?t=6m50s))

- Done by: chakilam
- Description: Create Bank Account Details, Credit/Debit card model, Notification model, views, controllers and apply validations and unit tests for each model. Associations between Organizations, Donors through Notifications & between Bank Accounts, Cards too their respective user models.  

### Goal: Tag and Payment implement ([2:35-6:49](https://youtu.be/-OZMbSu6uRU?t=2m35s))

- Done by: ashah1
- Description: Create Tag, DonorTag, OrganizationTag model and create associations between them has_many through and implement payment by organization side using Tag based organizatons. Whatever tag selected by donor. Edit donor and payment controllers and apply unit tests for each.
