# Individual Assignment Specifications

- Team: Donation Management Website
- Iteration: 2

## Special Roles

- Project Coordinator: Aakash Shah
- Quality Assurance Czar: Adithya Chakilam
- Video Demo Creators:
  - Ashlesh Gawande 50%
  - Aakash Shah 50%
- Demo-Booth Operator:  Adithya Chakilam

## Tasks: Ashlesh Gawande

### Task 1: Add Bootstrap UI
- Description: Add UI to all the pages to replace the basic UI.
- How to Evaluate: Evaluator may check various pages to see that all the basic UI has been replaced by a bootstrap UI.
- Outcome of Task: Bootstrap UI was implemented for every page in the application and customized as necessary.

### Task 2: Add Search functionality
- Description: Add search functionality so that a Donor can search Organizations. Integrate this functionality with website home page and Donor home page. Provide unit test to test the functionality. The search function should match [donor home page interface](../interface_designs/donor/donor_userpage.svg)
- How to Evaluate: Evaluator may go to the website home page or Donor home page and manually search the organization. Evaluator may also run the functional test for search controller.
- Outcome of Task: Search function was added to the nav bar itself (so that it is available for donor and anyone else) so any string can be searched to match against all organization attributes. Tag based search was implemented to search organizations related to certain tags. Search controller and test were added for this purpose.

## Tasks: Adithya Chakilam

### Task 3: Add model class for Notification
- Description: Notification model class should be added with Organization as foreign key as described in the [class diagram](../class_diagrams/class_associations.pdf). Add seed data, validations, and unit tests.
- How to Evaluate: Evaluator may look at the generated model class and match it with the class diagram. Evaluator may further check the validations, seed data, and unit tests.
- Outcome of Task: An organization can now send notifications to all donors in the system. Unit tests and controller tests were according to the good design principles.

### Task 4: Add model class for Bank Details
- Description: Bank Details model class should be added with Organization as foreign key as described in the [class diagram](../class_diagrams/class_associations.pdf). Add seed data, validations, and unit tests.
- How to Evaluate: Evaluator may look at the generated model class and match it with the class diagram. Evaluator may further check the validations, seed data, and unit tests.
- Outcome of Task: An organization can now add a bank account to his profile. Strict validations are defined to make sure that they are vaild US bank accounts. Unit tests and controller tests were according to the good design principles.

### Task 5: Add model class for Credit Cards
- Description: Credit Card model class should be added with Donor as foreign key as described in the [class diagram](../class_diagrams/class_associations.pdf). Add seed data, validations, and unit tests.
- How to Evaluate: Evaluator may look at the generated model class and match it with the class diagram. Evaluator may further check the validations, seed data, and unit tests.
- Outcome of Task:  An Donor can now add a Credit/Debit to his profile. Strict validations are defined to make sure that they are vaild cards using the help of external gems. Unit tests and controller tests were according to the good design principles.

## Tasks: Aakash Shah

### Task 6: Add Tag model and Reference to Donor and Organization model
- Description: Add the Tag model as shown in the class diagram and create the reference between Tag and Donor model as well as add controller logic required by the functionality of Tag Model and add organization editTag page as show in the [organization editTag interface](../interface_designs/organization/org_edit_tags.svg) as shown in the class diagram.
- How to Evaluate: Evaluator may verify the functionality of Tag model is working correctly and the reference between Tag and Donor is made as per the design principles and verify that Organization edit Tag page is displayed correctly as per the views and perform edit and delete tags in organization..
- Outcome of Task: A tag and donor can be associate with has_many relationship through donorTags table and same functionality implemented in organization and Tag through OrganizationTag table.So,Organization and Donor can select multiple tags by multiple selection. And update the organizationProfile and show page to visible Tags whatever selected by organization as well as update Donorprofile show page.

### Task 7: Edit donor homepage to show organizations 
- Description: Add organization basic informations in donor homepage as shown in the [donor homepage](../interface_designs/donor/donor_userpage.svg) 
- How to Evaluate: Evaluator may verify the organization details are displayed as per the design principles.
- Outcome of Task: A donor selects whatever tags by creating or editing profile on those tags basis the organizations can be shown in [donor home page interface](../interface_designs/donor/donor_userpage.svg)

