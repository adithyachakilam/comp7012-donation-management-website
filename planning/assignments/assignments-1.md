# Individual Assignment Specifications

- Team: Donation Management Website
- Iteration: 1

## Special Roles

- Project Coordinator: Ashlesh Gawande
- Quality Assurance Czar: Aakash Shah
- Video Demo Creators:
  - Ashlesh Gawande, 50%
  - Aakash Shah, 50%
- Demo-Booth Operator: Adithya Chakilam

## Tasks: Ashlesh Gawande

### Task 1: Create user Login and Register models
- Description: Using Devise provide the user a way to login and register. Override the model to include a user type as specified in the [class diagram](../class_diagrams/class_associations.pdf). Add validations for models. For Devise generated models override validations wherever required. Add unit tests for validations for models. Add seed data for unit tests. Add seed data for the User database so that there are some sample users.
- How to Evaluate: Evaluator may check that the generated models and validations are correct and are according to the class diagram. Evaluator may check the seed data and run the unit tests.
- Outcome of Task: Successfully completed. There were some changes in the class diagram where we eliminated an extra table and added a role attribute to the Devise User. Unit tests are added for validating the role.

### Task 2: Create UserType model
- Description: Create the UserType model such that it is a foreign key in Devise's User model as specified in [class diagram](../class_diagrams/class_associations.pdf). Add validation, seed, and unit tests.
- How to Evaluate: Evaluator may check that the generated models are correct and are according to the class diagram. Evaluator may further check the validations, seed data, and unit tests.
- Outcome of Task: Scrapped in favor of adding role attribute to User model.

### Task 3: Add views for the Login and Register
- Description: Create a home page view and add login to that home page as seen in [home user interface](../interface_designs/home.svg). Add register view as seen in [register interface](../interface_designs/registration.svg). Any validation failure must be handled gracefully in the UI.
- How to Evaluate: Evaluator may look at the home page and register page and match them with the given interfaces. Evaluator may try giving incorrect input and check that pages don't crash ungracefully.
- Outcome of Task: Successfully override Devise's views for customization. Register UI was updated
to remove Name field from it due to technical difficulty of applying different name validations for organization and donor on the same register page. Apart from that the implemented UIs are exactly as shown in interface_designs and errors are handled gracefully.

### Task 4: Override and add controller logic for User Login/Register
- Description: Override controller logic as required for the register page. Add logic to redirect upon login to the appropriate user page.
- How to Evaluate: Evaluator may check that the registration page works by trying to register and then logging in to check the redirection.
- Outcome of Task: Successfully override the registration controller to redirect to Donor or Organization.
Added a home controller and redirect to correct home page for the user. Added unit test to check this behavior.

### Task 5: Add model class for Notification
- Description: Notification model class should be added with Organization as foreign key as described in the [class diagram](../class_diagrams/class_associations.pdf). Add seed data, validations, and unit tests.
- How to Evaluate: Evaluator may look at the generated model class and match it with the class diagram. Evaluator may further check the validations, seed data, and unit tests.
- Outcome of Task: Moved to the next iteration and re-assigned.

## Tasks: Adithya Chakilam

### Task 6: Create organization model
- Description: Add the organization model as shown in the class diagram.
- How to Evaluate: Evaluator may verify that the built Organization model class has met the requirements as per the UML class diagram.
- Outcome of Task: Successfully completed. There were some changes in the class diagram where we eliminated an extra table and added a role attribute to the Devise User. Unit tests are added for validating the role.

### Task 7: Add views and verification(Handle Errors) for the organization
- Description: Add the view profile page, edit profile page for organization as described in the UI Sketches. Errors in the edit profile page are handled by flashing the notices and redirect to same page again.
- How to Evaluate: Evaluator may verify the functionality different views (pages) under the Organization model and may also verify that the edit organization profile page take cares of the validation rules errors.
- Outcome of Task: Successfully created Organization's views like new Organization and edit profile of organization and apply verifications on views.Besides that the implemented UIs for organizations are exactly as shown in interface_designs.

### Task 8: Add validations and unit tests for Organization model
- Description: To add the validations and unit tests of the attributes present in Organization as per the good design principles.
- How to Evaluate: Evaluator may verify that standard validation are applied in the Organization model as per design rules and can also run the unit tests of the model.
- Outcome of Task: Successfully completed and unit tests are built for validating every attributes of Organizations.

### Task 9: Add controllers for organization
- Description: Add controller logic as required by the functionality of Organization Model
- How to Evaluate: Verify that the Organization profile page, edit profile page and new post page are displayed correctly as per the views
- Outcome of Task: Succesfully built the controllers for all the views of the Organization and done the redirections as planned between all the views.

### Task 10: Create NewPost model
- Description: Add the NewPost model as shown in the class diagram.
- How to Evaluate: Evaluator may verify the functionality of create NewsPost is working correctly.
- Outcome of Task: Completion of the task lead to succesfull creation of a newspost by an organization. (An Image can also be included to a newspost) 

### Task 11: Add NewsPost Reference
- Description: Create the reference between NewsPost and Organization model as shown in the class diagram.
- How to Evaluate: Evaluator may verify that the reference between NewsPost and Organization is made as per the design principles
- Outcome of Task: By implementing this task, Every Newspost belongs to a specific organization. 

### Task 12: NewsPost Seed Data
- Description: Create the reference between NewsPost and Organization model as shown in the class diagram.
- How to Evaluate: Evaluator may verify the created seed data for the NewsPost model
- Outcome of Task: Suceffully added the seed data for newspost. (Title, Content and Image)

## Tasks: Aakash Shah

### Task 13: Create Donor model
- Description: Add the donor model as shown in the class diagram.
- How to Evaluate: Evaluator may verify that the built Donor model class has fulfilled the requirements as per the UML class diagram.
- Outcome of Task: Successfully completed.

### Task 14: Add views and verifications(Handle Errors) for the Donor
- Description: Add donor userpage,edit profile page,edit and create donation for donor as described in the UI Sketches and errors in these pages handled by flashing the notices and redirect to same page again.
- How to Evaluate: Evaluator may verify the functionality different views (pages) under the Donor model and may also takes care of the validation rules errors.
- Outcome of Task: Successfully created Donor's views like new donor and edit profile of donor and apply verifications on view like invalid DateFormat Type.Besides that the implemented UIs for Donor are exactly as shown in interface_designs.

### Task 15: Add validations and unit tests for Donor model
- Description: To add the validations of the attributes present in Donor as per the good design principles and add unit test cases of the particular validations in Donor model.
- How to Evaluate: Evaluator may verify that standard validation are applied in the Donor model as per design rules and execute unit tests.
- Outcome of Task: Successfully completed and unit tests are added for validating the attributes of Donor.

### Task 16: Add controllers for Donor
- Description: Add controller logic as required by the functionality of Donor Model
- How to Evaluate: Verify that the Donor user page, edit profile page,edit and create donation page are displayed correctly as per the views.
- Outcome of Task: Successfully created and redirects to the different actions of Donor. Added unit test to check the behavior of all actions in that controller.

### Task 17: Create Payment model and Reference to Donor model
- Description: Add the Payment model as shown in the class diagram and create the reference between Payment and Donor model as shown in the [class diagram](class_diagrams/class_associations.pdf).
- How to Evaluate: Evaluator may verify the functionality of payment model is working correctly and the reference between Payment and Donor is made as per the design principles.
- Outcome of Task: Successfully created and generate association betweeen Payment and module and unit tests are added to validate the amount of Payment.

### Task 18: Payment Seed Data
- Description: Create the reference between Payment and Donor model as shown in the class diagram.
- How to Evaluate: Evaluator may verify the created seed data for the Payment model.
- Outcome of Task: dynamically generated by donor when apply for payment.

### Task 19: Create Tag model and Reference to Donor model
- Description: Add the Tag model as shown in the class diagram and create the reference between Tag and Donor model as shown in the class diagram.
- How to Evaluate: Evaluator may verify the functionality of Tag model is working correctly and the reference between Tag and Donor is made as per the design principles.
- Outcome of Task: Moved to the next iteration.

### Task 20: Tag Seed Data
- Description: Create the reference between Tag and Donor model as shown in the class diagram.
- How to Evaluate: Evaluator may verify the created seed data for the Tag model.
- Outcome of Task: Filled with tag names.


### Unplanned Tasks

### Task 21: Reference Payment model to Organization model
- Description: Create the reference between Payment and Organization model as shown in the [class diagram](class_diagrams/class_associations.pdf) with the foriegn key.
- How to Evaluate: Evaluator may verify the functionality of the reference between Payment and Organization is made as per the design principles.
- Outcome of Task: Successfully created and generate association betweeen Payment and Organization module.
