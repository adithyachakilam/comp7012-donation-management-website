# Individual Assignment Specifications

- Team: Donation Management Website
- Iteration: 0

## Special Roles

- Project Coordinator: Ashlesh Gawande
- Quality Assurance Czar: Aakash Shah 

## Tasks: Adithya Chakilam

### Task 1: Editing the User Interface and Glossary
- Description: There were many changes to be made on the user stories and glossary from the feedback we have got from the peer activity session. So, I have worked on that.
- How to Evaluate: You can see that all the [userstories](../user_stories.md) and the [glossary](glossary.md) match the template given in the class and are well defined.
- Outcome of Task: A set of Well defined userstories, which will be used in the next iterations.

### Task 2: Creating User Interface Sketches
- Description: Creating the hand drawn sketches for the different views that are to rendered by the web application.
- How to Evaluate: You can check that the [UI sketches](../UI_Sketches.pdf) shows all the basic page elements and follow principles of the good design.
- Outcome of Task: A complete set of User Interface designs of all the pages of Web Application

## Tasks: Ashlesh

### Task 3: Creating the Model class association diagram
- Description: Create a model class association diagram for Donation Management that will show the relations between various entities.
- How to Evaluate: Evaluator may view the [class_associations.pdf](../class_diagrams/class_associations.pdf) and check whether model associations are correct i.e. they make logical sense and have the correct multiplicities.
- Outcome of Task: A class diagram is created that will be used by the team in generating models.

### Task 4: Digitizing the user interface diagrams
- Description: Digitize and organized the hand drawn diagrams to svg format to track them as we update. Hand drawing over and over again is slower and confusing.
- How to Evaluate: Evaluator may view the [interface_designs](../interface_designs) to assess the logical organization and quality of digitization of the interfaces.
- Outcome of Task: User interfaces are organized and can be easily read and updated by anybody.

## Tasks: Aakash

### Task 5: Creating the SiteMap for web application
- Description: Create a sitemap for Donation management that will show the how all web pages are   connected and what will be the flow of the website
- How to Evaluate: Evaluator may view the [interface_designs](../interface_designs) for evaluate the sitemap that pages communicate with each other in flow.
- Outcome of Task: sitemap is organized according to the flow of the Donation Management website and can be very easy to understand and read.

### Task 6: Make repository clean and Formatting the files (Task Name)
- Description: clean the repository of Donation Management website by removing some unnecessary files and format the files with standard format.  
- How to Evaluate: Evaluator may view the entire repository and all files those are very well arranged. 
- Outcome of Task: easily understand and to get the structure of the the entire web application. 
