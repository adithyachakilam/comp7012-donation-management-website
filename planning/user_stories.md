# User Stories

**Team:**  Donation Management Website

## User Registration and Profile

- US User Registration
  - Description: As an User, I should be able to register as an Organization with the web app.
  - Dependencies: None
  - Estimate: 2.5
  - Priority: xxx 
  - Status: Incomplete
  - Author: Adithya

- US User Login
  - Description: As a user, I need to login to donate as a Donor or to recive funds as a Organization.
  - Dependencies: None
  - Estimate: 2.5
  - Priority: xxx 
  - Status: Incomplete
  - Author: Adithya

- US Editing Organization Profile
  - Description: As an Organization, I should be able to create and edit a profile associated with my account.
  - Dependencies: User Registration, Login 
  - Estimate: 3
  - Priority: xxx 
  - Status: Incomplete
  - Author: Adithya

- US Adding Organization NewsFeed 
  - Description: As an Organization, I should be able to add news items/stories to my profile.
  - Dependencies: User Registration, Login
  - Estimate: 4
  - Priority: xxx 
  - Status: Incomplete
  - Author: Adithya

- US Listing Donors
  - Description: As an Organization, I should be able see the number of Donors and total donation per month.
  - Dependencies: User Registration, Login
  - Estimate: 3
  - Priority: xxx 
  - Status: Incomplete
  - Author: Aakash

- US Adding Organization Profile Tags 
  - Description: An Organization will be able to add relevant tags to its profile.
  - Dependencies: User Registration, Login
  - Estimate: 2
  - Priority: xxx 
  - Status: Incomplete
  - Author: Aakash

- US Editing Donor Profile
  - Description: As a Donor, I will be able to create a profile with relevant personal information which is not be public and shown only to the Organization for which I donated.
  - Dependencies: User Registration, Login
  - Estimate: 3
  - Priority: xxx 
  - Status: Incomplete
  - Author: Aakash

- US Adding Donor Profile Tags
  - Description: A Donor will be able to add tags to the profile to find relevant Organizations.
  - Dependencies: User Registration, Login
  - Estimate: 3
  - Priority: xxx 
  - Status: Incomplete
  - Author: Aakash

## Organization and Donor discovery

- US Suggesting Organizations
  - Description: A Donor will be able to see a list of Organizations upon login based on profile tags.
  - Dependencies: User Registration, Login
  - Estimate: 3
  - Priority: xxx 
  - Status: Incomplete
  - Author: Aakash

- US Searching Organizations
  - Description: As a Donor, I will be able to search the various Organizations using a search box.
  - Dependencies: User Registration, Login
  - Estimate: 4
  - Priority: xxx 
  - Status: Incomplete
  - Author: Ashlesh

- US Listing Organizations
  - Description: As a Donor, I should be able to see the list of Organizations to which I am currently donating.
  - Dependencies: User Registration, Login
  - Estimate: 3 
  - Priority: xxx 
  - Status: Incomplete
  - Author: Ashlesh

## Donating and Receiving Funds

- US Creating Funds
  - Description: As a Donor, I should be able to donate specified funds to an Organization on a recurring basis.
  - Dependencies: User Registration, Login
  - Estimate: 3
  - Priority: xxx 
  - Status: Incomplete
  - Author: Ashlesh

- US Receiving Funds
  - Description: As an Organization, I should be able to receive funds from Donors.
  - Dependencies: User Registration, Login
  - Estimate: 3
  - Priority: xxx 
  - Status: Incomplete
  - Author: Ashlesh

- US Adding PayPal Information
  - Description: As a Donor, I want to add my PayPal payment information to my account, so I can pay for donations
  - Dependencies: User Registration, Login
  - Estimate: 3
  - Priority: xxx 
  - Status: Incomplete
  - Author: Adithya

## Advertising 

- US Advertising Organization
  - Description: As an Organization, I should be able to advertise myself to donors.
  - Dependencies: User Registration, Login
  - Estimate: 3
  - Priority: xxx 
  - Status: Incomplete
  - Author: Ashlesh
